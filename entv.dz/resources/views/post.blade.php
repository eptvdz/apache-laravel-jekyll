@extends('default')

@section('title')
  {{ $post->title }}
@endsection

@section('content')
  {{ $post->content }}
@endsection
