@extends('default')

@section('title')
  EPTV News
@endsection

@section('content')
  <ul>
    @foreach ($posts as $post)
      <li>
        <a href="{{ route('post', ['id' => $post->id]) }}">{{ $post->title }}</a>
      </li>
    @endforeach
  </ul>
@endsection
