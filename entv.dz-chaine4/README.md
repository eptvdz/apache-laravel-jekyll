# Exemple de site web

# Documentations de référence

## Jekyll

Variables principales :

- `site.pages`
- `site.posts`
- `content`

## Liquid

Contrôle des variables, boucles, conditions etc.

> https://shopify.github.io/liquid/

```
{% if news_flash %}
  ...
{% endif %}
```

Pour afficher une variable, utiliser les `{{ }}` :

```
{{ site.posts[0].title }}
```
