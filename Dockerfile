FROM php:7-apache

RUN docker-php-ext-install pdo pdo_mysql \
  && a2enmod rewrite \
  && a2enmod headers \
  && a2enmod expires
